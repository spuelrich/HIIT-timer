/*
 * 
 * HIIT Timer - A simple timer for high intensity trainings
 Copyright (C) 2015 Lorenzo Chiovini

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

package org.hiittimer.database.generated;

import java.util.Map;

import android.database.sqlite.SQLiteDatabase;
import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.AbstractDaoSession;
import de.greenrobot.dao.identityscope.IdentityScopeType;
import de.greenrobot.dao.internal.DaoConfig;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * {@inheritDoc}
 * 
 * @see de.greenrobot.dao.AbstractDaoSession
 */
public class DaoSession extends AbstractDaoSession {

    private final DaoConfig trainingPlanDaoConfig;
    private final DaoConfig roundDaoConfig;

    private final TrainingPlanDao trainingPlanDao;
    private final RoundDao roundDao;

    public DaoSession(SQLiteDatabase db, IdentityScopeType type, Map<Class<? extends AbstractDao<?, ?>>, DaoConfig>
            daoConfigMap) {
        super(db);

        trainingPlanDaoConfig = daoConfigMap.get(TrainingPlanDao.class).clone();
        trainingPlanDaoConfig.initIdentityScope(type);

        roundDaoConfig = daoConfigMap.get(RoundDao.class).clone();
        roundDaoConfig.initIdentityScope(type);

        trainingPlanDao = new TrainingPlanDao(trainingPlanDaoConfig, this);
        roundDao = new RoundDao(roundDaoConfig, this);

        registerDao(TrainingPlan.class, trainingPlanDao);
        registerDao(Round.class, roundDao);
    }
    
    public void clear() {
        trainingPlanDaoConfig.getIdentityScope().clear();
        roundDaoConfig.getIdentityScope().clear();
    }

    public TrainingPlanDao getTrainingPlanDao() {
        return trainingPlanDao;
    }

    public RoundDao getRoundDao() {
        return roundDao;
    }

}
